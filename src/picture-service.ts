
import axios from 'axios';

export async function fetchPictures() {
  const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/picture');
  console.log("from fetchPictures");
    return response.data
}