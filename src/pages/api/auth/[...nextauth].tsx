import NextAuth from "next-auth"
import CredentialsProvider from "next-auth/providers/credentials"
import axios from 'axios';

export default NextAuth({
    jwt: {
      secret: process.env.JWT_SECRET
    },
    callbacks: {
        async jwt({ token, user }) {
          if (user) {
            return {
              accessToken: user.token,
              user: user.user,
            }
          }
    
          return token;
        },
        async session({ session, token }) {
          session.accessToken = token.accessToken;
          session.user = token.user as any;
          return session;
        }
      },
  providers: [
    CredentialsProvider({
      name: "Credentials",
      credentials: {
        username: { label: "Username", type: "text", placeholder: "Your username..." },
        password: { label: "Password", type: "password", placeholder: "Your Password..." }
      },
      async authorize(credentials, req) {
        try {
          const response = await axios.post("http://localhost:8000/api/user/login", credentials);
          const data = response.data;
          if (data.user && data.token) {
            return data;
          }
          return null;
        } catch (error) {
          console.log("error");
          return null;
        }
      }
    })
  ],
})