
import { Picture } from "./Picture";


export const PictureList = ({list}:any) => {


    return (
        <section>

            {list.map((item:any) => <Picture key={item.id} picture={item} />)}

        </section>
    );
}